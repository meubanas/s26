// 1. What directive is used by Node.js in loading the modules it needs?

answer In order to load the http module and store the returned HTTP instance into an http variable we make use of the require directive. By using the created http instance we call the http. createServer() method to create a server instance. Then, we bind it at port 8080 with the listen method related with the server instance

// 2. What Node.js module contains a method for server creation?

answer Using http. createServer() method includes request and response parameters which is supplied by Node. js. The request object can be used to get information about the current HTTP request e.g., url, request header, and data.

// 3. What is the method of the http object responsible for creating a server  using Node.js?

answer listen(4000)

// 4. What method of the response object allows us to set status codes and content types?

answer response.writeHead(200, {'Content-Type': 'text/plain'});

// 5. Where will console.log() output its contents when run in Node.js?

answer terminal

// 6. What property of the request object contains the address's endpoint?

answer url


