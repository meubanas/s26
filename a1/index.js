const http = require('http');

const PORT = 3000;

http.createServer(function(request, response){

	if(request.url ==  '/login'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page.')

	}else if(request.url ==  '/register'){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Iim sorry the page you are looking for cannot be found')

	} else {

		response.writeHead(404,{'Content-Type': 'text/plain'})
		response.end('Page Not Found')	
			
	}

}).listen(PORT)

console.log(`Server is running at localhost:${PORT}.`);